import React, {useContext} from 'react';
import TodoItem from '../TodoItem'
import { TodoListStore } from '../../../../contexts/todolist.context';

const ListWithItems = () => {
    const TodoStore = useContext(TodoListStore);
    const { todos } = TodoStore
    return(
        <div>
                {todos.length ? todos.map((todo) => (
                    <TodoItem key={todo.id} cross={todo.cross} id={todo.id} description={todo.description} checked={todo.checked}/>
                )) : null}
        </div>
    )
}
export default ListWithItems
