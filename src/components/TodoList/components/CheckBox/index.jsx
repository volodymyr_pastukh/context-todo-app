import React, {memo} from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import styles from './style.module.sass'
const CheckBox = memo(({onChange, checked}) => {
    return(
        <div className={styles.checkBox}>
            <Checkbox onChange={onChange} checked={checked}/>
        </div>
    )
})

export default CheckBox
