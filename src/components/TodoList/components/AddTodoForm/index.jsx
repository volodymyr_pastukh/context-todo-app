import React, {useState, useEffect, useContext} from 'react';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';

import styles from './style.module.sass'
import {TodoListStore} from "../../../../contexts/todolist.context";
const AddTodoForm = () => {
    const [value, setValue] = useState('')
    const [valueError, setValueError] = useState('')
    const TodoStore = useContext(TodoListStore);
    const { actionTodos } = TodoStore

    const createTodoHandler = () => {
        if(value.length < 10) {
            setValueError('Enter more than 10 characters');
            return
        }
        actionTodos({ type: 'add_todo_item', payload: {
            id: new Date().getUTCMilliseconds(),
            description: value,
            checked: false
            }})
        setValueError('')
        setValue('')
    }
    const changeInputField = (e) => {
        setValue(e.target.value)
    }
    const createTodoHandlerEnter = (e) => {
        if(e.key === 'Enter') {
            createTodoHandler()
        }
    }

    return(
        <div className={styles.addTodoForm}>

            <FormControl className={styles.addTodoForm_formControll}>
                <TextField onKeyDown={createTodoHandlerEnter} error={Boolean(valueError.length)} value={value} onChange={changeInputField} id="outlined-basic" label="New todo" variant="outlined" color="secondary" />
                <FormHelperText error={Boolean(valueError.length)} id="my-helper-text">{valueError}</FormHelperText>
            </FormControl>
            <Button onClick={createTodoHandler} variant="contained" style={{height: '56px', marginLeft: '15px', whiteSpace: 'nowrap',}} size="large" color="secondary" >
                Add
            </Button>
        </div>
    )
}


export default AddTodoForm
