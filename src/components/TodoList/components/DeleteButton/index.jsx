import React, {memo} from 'react';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';

import styles from './style.module.sass'
const DeleteButton = memo(({onClick}) => {
    return(
        <div className={styles.deleteButton}>
            <IconButton onClick={onClick} color="secondary" aria-label="delete">
                <DeleteIcon fontSize="small" />
            </IconButton>
        </div>
    )
})


export default DeleteButton
