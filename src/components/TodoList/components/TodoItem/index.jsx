import React, {useContext, useEffect, useRef, useState, memo} from 'react';
import styles from './style.module.sass'
import CheckBox from "../CheckBox";
import DeleteButton from '../DeleteButton'
import Tooltip from '@material-ui/core/Tooltip';
import {TodoListStore} from "../../../../contexts/todolist.context";

const TodoItem = memo(({id, checked, description}) => {
    const changeCheckbox = () => {
        actionTodos({
            type: 'check_todo_item',
            payload: id
        })
    };
    const deleteTodoItemHandler = () => {
        actionTodos({
            type: 'remove_todo_item',
            payload: id
        })
    }
    const TodoStore = useContext(TodoListStore);
    const { actionTodos } = TodoStore
    const descriptionRef = useRef();
    const [tooltip, setAddTooltip] = useState(false)
    useEffect(() => {
        const description = descriptionRef.current
        if(description && description.offsetWidth < description.scrollWidth) {
            setAddTooltip(true)
        }
    })

    return(
        <div className={styles.todoItem} >
            <CheckBox onChange={changeCheckbox} checked={checked}/>
            <div className={styles.todoItem_content}>
                {tooltip ? (
                    <Tooltip title={description} aria-label="add">
                        <p ref={descriptionRef}>
                            {description}
                        </p>
                    </Tooltip>
                ) : (
                    <p ref={descriptionRef}>
                        {description}
                    </p>
                )}
            </div>
            <DeleteButton onClick={deleteTodoItemHandler}/>
        </div>
    )
})

export default TodoItem
