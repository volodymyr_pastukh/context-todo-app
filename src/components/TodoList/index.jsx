import React from 'react';
import styles from './style.module.sass'
import ListWithItems from './components/ListWithItems'
import AddTodoForm from "./components/AddTodoForm";
import {TodoListProvider} from '../../contexts/todolist.context'

const TodoList = () => {
    return(
        <div className={styles.todoList}>
            <h1>ToDoo List</h1>
            <TodoListProvider>
                <ListWithItems />
                <AddTodoForm/>
            </TodoListProvider>
        </div>
    )
}
export default TodoList
