import React, {createContext, useReducer, useState} from 'react';

const initialState = [
    {
        id: 12,
        description: 'SAdasd assdasd asdasda asdasd',
        checked: false,
    },
    {
        id: 1,
        description: 'SAdasd ',
        checked: false,
    }
];

const TodoListStore = createContext(initialState);
const { Provider } = TodoListStore;

const TodoListProvider = ( { children } ) => {
    const [timer, setTimer] = useState(false)

    const [todos, actionTodos] = useReducer((state, action) => {
        switch(action.type) {
            case 'add_todo_item':
                console.log('ADD', action.payload)
                const newState = [
                    ...state,
                    action.payload
                ]
                return newState;
            case 'remove_todo_item':
                state = state.filter(function( obj ) {
                    return obj.id !== action.payload;
                });
                return [
                    ...state
                ];
            case 'check_todo_item':
                // const checkObject = state.findIndex((obj => obj.id == action.payload));
                // const checkObject = state.find((obj => obj.id == action.payload));
                const updatedItems = state

                updatedItems.forEach((element, index) => {
                    if(element.id == action.payload && !timer) {
                        if(updatedItems[index].checked) {
                            updatedItems[index].checked = false
                        } else {
                            updatedItems[index].checked = true;
                        }
                        setTimer(true)
                        setTimeout(() => {
                            setTimer(false)
                        }, 100)
                    }
                });

                return [
                    ...updatedItems,
                ];
            default:
                throw new Error();

        };
    }, initialState);

    return <Provider value={{ todos, actionTodos }}>{children}</Provider>;
};

export { TodoListStore, TodoListProvider }
