import React from 'react';
import TodoList from './components/TodoList'
import Container from '@material-ui/core/Container';

import './sass/global.sass'
function App() {
  return (
    <div className="App">
        <Container style={{height: '100%', display: 'flex', alignItems: 'center'}} maxWidth="sm">
                <TodoList />
        </Container>
    </div>
  );
}

export default App;
